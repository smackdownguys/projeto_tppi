##
# \author Marcos Ramos
# \author Flavio Pereira
# \date 27/11/19
# \bug Não há erros detetados
# \warning Introduzir apenas numeros inteiros
# \version 1.0
#
##
# \brief Base ^ expoente
# \details Calcula o valor de uma potência com base nos valores introduzidos pelo o utilizador, nomeadamente base e expoente
# \param n : Valor da base
# \param e : Valor do expoente
# \return Retorna o valor da potência
def potencia():
    n = int(input("Introduza o valor da base: "))
    e = int(input("Introduza o valor do expoente: "))
    print(n,"^",e, "=",n**e)
potencia()
