##
# \author Marcos Ramos
# \author Flávio Pereira
# \date 27/11/19
# \version 1.0
# \bug Zero erros
# \warning Apenas colocar numeros inteiros dentro das listas

lista = []
parlista = []
imparlista = []

##
# \brief Criar lista
# \details Função para criar uma lista com numeros iseridos utilizador através de um ciclo "for"
# \param x : indice de 0 até 9
# \return Retorna uma lista organizada de for ascendente
# \sa diferenca Utiliza os dados introduzidos na lista criada previamente e realiza a diferença entre o maior e o menor numero introduzido
# \sa par_impar Utiliza os dados introduzidos na lista criada previamente e separa os numeros impares dos pares
def lista_val():
    for x in range(0, 10):
        lista.append(int(input('Introduza 10 números: ')))
        print('-=' * 21)
    lista.sort()
    print(lista)
lista_val()

##
# \brief Maior - Menor
# \details Função Utiliza os dados introduzidos na lista criada previamente e realiza a diferença entre o maior e o menor numero introduzido
# \param lista[9] : maior numero da lista 
# \param lista[0] : menor numero da lista
# \return Retorna o valor da diferença entre o maior e o menor numero da lista
# \sa lista_val Função para criar uma lista com numeros iseridos utilizador através de um ciclo "for" 
# \sa par_impar Utiliza os dados introduzidos na lista criada previamente e separa os numeros impares dos pares
def diferenca():
    print('O maior número: ', lista[9])
    print('O menor número: ', lista[0])
    print(lista[9], '-', lista[0], '=', lista[9]-lista[0])
diferenca()

##
# \brief ParImpar
# \details Função Utiliza os dados introduzidos na lista criada previamente e separa os numeros impares dos pares
# \param parlista : lista com os numeros pares 
# \param imparlista : numeros com numeros impares
# \return Uma lista organizada de forma ascendente de numeros pares e uma lista organizada de forma ascendente de numeros impares
# \sa lista_val Função para criar uma lista com numeros iseridos utilizador através de um ciclo "for"
# \sa diferenca Utiliza os dados introduzidos na lista criada previamente e realiza a diferença entre o maior e o menor numero introduzido
def par_impar():
    for i in range(len(lista)):
        if lista[i] % 2 == 0:
            parlista.append(lista[i])
        else:
            imparlista.append(lista[i])
    print('Números pares é: ', parlista)
    print('Números ímpares é: ', imparlista)
par_impar()
