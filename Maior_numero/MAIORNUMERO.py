##
# \author Marcos Ramos
# \author Flavio Pereira
# \date 27/11/19
# \bug Não há erros detetados
# \warning Introduzir apenas numeros inteiros
# \version 1.0
#
##
# \brief Maior numero contido num algarismo
# \details Após a introdução de um numero com tamanho indeterminado desde que inteiro, o programa utiliza a função "map" ler e separar o numero introduzido, transformando-o em uma lista
# \param maior : maior algarismo 
# \param c : variavel contador 
# \return Retorna o maior algarismo contido no numero inserido 
num = input('Introduza um número: ')

def maior_algarismo():
   maior = 0
   for c in map(int, num):
        if c > maior:
            maior = c
        elif maior == 9:
            break
   print('O maior algarismo contido no número',num, 'é o', maior)

maior_algarismo()
